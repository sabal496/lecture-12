package com.example.lecture12

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycle_layout.view.*

class adapter(val ContentList:MutableList<Mymodel>/*,private val itemClick:DeleteClick*/):RecyclerView.Adapter<adapter.viewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {

        return viewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycle_layout,parent,false))
    }

    override fun getItemCount(): Int {

        return ContentList.size

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        holder.onbind()

    }

    inner class viewHolder(view:View):RecyclerView.ViewHolder(view)/*,View.OnClickListener*/{

     /*   override fun onClick(v: View?) {
             ContentList.removeAt(adapterPosition)
             notifyDataSetChanged()
        }
*/

        lateinit var  model:Mymodel
        fun onbind(){
            model=ContentList[adapterPosition]
            itemView.image.setImageResource(model.Image)
            itemView.title.text=model.title
            itemView.describe.text=model.desc

            itemView.items.setOnLongClickListener{

                ContentList.removeAt(adapterPosition)
                notifyDataSetChanged()

               /* var  intent=Intent(itemView.context,MainActivity::class.java)
                 intent.putExtra("position",adapterPosition)
                itemView.context.startActivity(intent)
*/
                true

            }



        }




    }


}