package com.example.lecture12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.recycle_layout.*

class MainActivity : AppCompatActivity() {

    val ContentList = mutableListOf<Mymodel>()
    private lateinit var adapt:adapter
    private lateinit var itemclick:DeleteClick

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

    }

    private  fun init(){

        adapt= adapter(ContentList)
        mainLayout.layoutManager=LinearLayoutManager(this)
        mainLayout.adapter=adapt
        setdate()

/*
        var intent=intent
        var position=intent.getIntExtra("position",1)
        intent.removeExtra("position")
        ContentList.removeAt(position)
        adapt.notifyItemRemoved(position)*/






          /* itemclick=object : DeleteClick{
            override fun click(posit: Int) {
                ContentList.removeAt(posit)
                adapt.notifyItemRemoved(posit)
            }

        }*/

        addbtn.setOnClickListener(){

            setdate()
            adapt.notifyItemInserted(0)
            mainLayout.scrollToPosition(0)
            d("siz",ContentList.size.toString())
        }


    }

    private  fun setdate(){

         val randomContext= mutableListOf<Mymodel>()

         var model1=Mymodel(R.mipmap.kotlin,"Kotlin","programing language")
         val model2=Mymodel(R.mipmap.c,"C","programing language")
         val model3=Mymodel(R.mipmap.python,"Python","programing language")
         val model4=Mymodel(R.mipmap.swift,"Swift","programing language")
         val model5=Mymodel(R.mipmap.ruby,"Ruby","programing language")

        randomContext.add(model1)
        randomContext.add(model2)
        randomContext.add(model3)
        randomContext.add(model4)
        randomContext.add(model5)


        ContentList.add(0,randomContext[(randomContext.indices).random()])
    }



}
